import requests
import string
from os import path, mkdir, sep
from bs4 import BeautifulSoup


class WebScraper:
    PAGES_INPUT = "Enter the number of pages\n> "
    TYPE_INPUT = "Enter the type of articles:\n" \
                 "common values: ['News', 'Research Highlight', 'Editorial']\n> "

    def __init__(self):
        self.response = None
        self.soup = None
        self.url = None

        self.article_title = ""
        self.article_content = ""
        self.pages = None
        self.article_type = None
        self.saved_articles = []

    def run_program(self) -> None:
        """Main functionality"""
        try:
            # if self.url is None:
            #     url = input("Input the URL:\n")
            #     self.set_url(url)
            #
            # if self.response is None:
            #     self.set_response()
            #
            # if self.soup is None:
            #     self.set_soup()

            if self.article_type is None or self.article_type is None:
                self.pages = int(input(self.PAGES_INPUT))
                self.article_type = input(self.TYPE_INPUT)

            print(self.article_scrap())

        except ValueError as e:
            print(e)
        except TypeError as e:
            print(e)

    def set_url(self, url) -> None:
        self.url = url

    def set_response(self) -> None:
        # english configuration
        self.response = requests.get(self.url, headers={
            'Accept-Language': 'en-US,en;q=0.5'})

        if self.response.status_code != 200:
            status_message = f"The URL returned {self.response.status_code}!"
            self.response = None
            raise ValueError(status_message)

    def set_soup(self) -> None:
        self.soup = BeautifulSoup(self.response.content, 'html.parser')

    def save_file(self, page) -> None:
        try:
            folder = 'Articles/Page_' + str(page)
            if not path.exists(folder):
                mkdir(folder)
            f = open(folder + sep + self.article_title + ".txt", 'wb')
            page_content = self.article_content.encode()
            f.write(page_content)
            f.close()
        except TypeError:
            raise

    @staticmethod
    def clean_title(txt) -> str:
        clean_title = []

        for word in txt.split():
            if word in string.punctuation:
                continue
            clean_title.append(word.replace('?', ''))

        return "_".join(clean_title)

    def article_scrap(self) -> str:
        pages_url = "https://www.nature.com/nature/articles?searchType=journalSearch&sort=PubDate&year=2020&page="
        for p in range(1, self.pages + 1):
            self.url = pages_url + str(p)
            self.set_response()
            self.set_soup()

            articles = self.soup.find_all('article', {'class': 'u-full-height c-card c-card--flush'})
            for a in articles:
                a_type = a.find('div', {'class': 'c-card__section c-meta'}).find('span',
                                                                                 {'class': 'c-meta__type'}).text
                if a_type == self.article_type:
                    title_txt = a.find('h3').find('a').text
                    self.article_title = self.clean_title(title_txt)

                    # get the content of the article by following the link
                    a_url = 'https://www.nature.com' + a.find('a').get('href')
                    a_page = requests.get(a_url)
                    a_soup = BeautifulSoup(a_page.content, 'html.parser')
                    self.article_content = a_soup.find('div', {'class': 'main-content'}) \
                                           or a_soup.find('p', {'class': 'article__teaser'})
                    self.article_content = self.article_content.text if self.article_content is not None else ''

                    self.saved_articles.append(self.article_title)
                    self.save_file(p)

        return f"Saved articles:\n{self.saved_articles}"


if __name__ == "__main__":
    program = WebScraper()
    program.run_program()
