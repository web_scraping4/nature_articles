# Nature Articles
Script that downloads a set of articles from the Nature page based on the type specified by the user.
# Setup (linux)
- Inside the root directory (`nature_articles`), install `requirements.txt`:
    ```shell
    python -m venv venv && \
    source venv/bin/activate && \
    pip install -r requirements.txt
    ```
- Run the program
    ```shell
    python main.py
    ```
